Pod::Spec.new do |s|
 
  #设置组件库的名称
  s.name         = "ytx_sdk_test"  

  #设置组件库的版本号
  s.version      = "0.0.2" 

  #组件库的简介
  s.summary      = "容联云通讯IM SDK(如果看不到最新版本，请使用 pod repo update 命令更新一下本地pod仓库)" 

  # 组件库的详情描述，要求比简介的字数多些
  s.description  = <<-DESC
                  容联云通讯SDK YuntxIMLib. (如果看不到最新版本，请使用 pod repo update 命令更新一下本地pod仓库)
                   DESC
  #设置仓库主页
  s.homepage     = "https://www.yuntongxun.com/"

  #设置许可
  s.license      = "MIT"

 #设置作者
  s.author       = { "gaoyuan" => "2502905737@qq.com" }

 #设置支持的最低系统版本
  s.ios.deployment_target = "8.0"

 #设置仓库源,表示在哪可以找到组件工程
  s.source       = { :git => "https://gitlab.com/GG_bond/ytx_sdk_test.git", :tag => "#{s.version}" }
  
 #资源文件
  s.resources  = "sdk/CCPSDKBundle.bundle"

 #设置源文件路径，不是整个工程的文件，而是自己封装的需要暴露出来的代码，以后别的工程引入，就会引入这里的代码。
  s.source_files = "sdk/*.h"
 
 #设置使用的静态库(非系统)
  s.vendored_library = 'sdk/*.a'

  #设置依赖库(系统)，不需要 lib，例如： libicucore写成icucore即可
  s.libraries    = "resolv.9","icucore","sqlite3","z","xml2","bz2.1.0","c++"

 #设置依赖的 framework(系统)，写的时候不需要后缀名
  s.framework    = "CoreTelephony","MediaPlayer","CFNetwork","SystemConfiguration","MobileCoreServices","AudioToolbox","AVFoundation","VideoToolbox"

 #设置使用的framework(非系统)
 # vendored_frameworks = ""

 #设置子目录
  s.subspec 'Delegate' do |ss|
  ss.source_files = "sdk/Delegate/*.h"
  end

  s.subspec 'enums' do |ss|
  ss.source_files = "sdk/enums/*.h"
  end

  s.subspec 'Manager' do |ss|
  ss.source_files = "sdk/Manager/*.h"
  end

  s.subspec 'private' do |ss|
  ss.source_files = "sdk/private/*.h"
  end

  s.subspec 'types' do |ss|
  ss.source_files = "sdk/types/*.h"

  ss.subspec 'LiveChatRoomType' do |sss|
  sss.source_files = "sdk/types/LiveChatRoomType/*.h"
  end

  end

  s.subspec 'board' do |ss|
  ss.source_files = "sdk/board/*.h"
  end

  #设置组件库是否是基于 ARC 内存管理的，默认为 true，如果不是，会自动添加-fno-objc-arc 
  s.requires_arc = true

  #如果部分是ARC
  #spec.requires_arc = false
  #spec.requires_arc = ['Classes/*ARC.m', 'Classes/ARC.mm'] //指定 MRC 的文件

  #设置依赖的其他 pod 库
  # s.dependency "JSONKit", "~> 1.4"

  #设置xcconfig
  s.xcconfig = {
      'OTHER_LINKER_FLAGS' => '-ObjC',
      'ENABLE_BITCODE' => 'NO'
    }
end